Anagrams native mobile app using Kotlin + Rust
==============================================

This is a demonstration of building an end-to-end native Android mobile app using:

- [Rust programming language](https://www.rust-lang.org/) for primary
  application logic as a common library also deployed as a web service and
  command-line utility
- [Kotlin programming language](https://kotlinlang.org/) for Android native UI
- UI layout via Android XML files
- Build automation via Makefiles
  + Using files from [android-makefile-without-ide](https://gitlab.com/dpezely/android-makefile-without-ide)
  by same author
- For sample of *only* the files that are directly relevant to development
  of the end-to-end features, look at the *second* commit: [a10cfaa](https://gitlab.com/dpezely/native-android-kotlin-rust/-/commit/a10cfaa11eb15b2db1ece61cc9798b991d450357).
  The third commit adds files necessary for `gradlew` to actually build the
  Android project so that you have a complete set of files as of
  `Q`, API level 29
  being the current stable release of Android.

Status:

- Search query returns results screen (Activity) an API level 29 emulator
- There's an unresolved issue with character
  [encoding](https://docs.rs/jni/0.16.0/jni/struct.JNIEnv.html#method.new_string)
  between Rust's UTF-8 and Java's modified UTF-8 format.

What it's NOT:

- More work would be required before submitting to any app store, due to
  lack of code-signing, metadata, icons, etc.
- For other *complete* open source apps that also solve for anagrams,
  [search on F-Droid](https://search.f-droid.org/?q=anagrams)

To Do:

- Use a background thread since queries may take more than a few milliseconds
- Use `Service` so other apps can bind to this one

- Repeat for [Swift](https://swift.org/)-based iOS native UI
- Annotate Makefiles and instructions for macOS, Windows, BSD Unix
  + See also [android-makefile-without-ide](https://gitlab.com/dpezely/android-makefile-without-ide/)
- Automated testing
  + Time hasn't allowed for that learning curve here
  + Some manual behaviour tests were performed by modifying the primary code

(Contributors welcome!)

## Development Overview

There are common components shared between Android, iOS and web server
deployments.  The core library is implemented in Rust.

Each submodule lives within its own git repository and may be built
independently while accounting for dependencies.

Subprojects are based upon programming language and library functionality:

- [`anagram-phrases`](https://github.com/dpezely/anagram-phrases) included by
  reference into `android-rust` as library -- by same author
- [`android-rust`](android-rust/) as JNI wrapper around `anagram-phrases` package
- [`android-app`](android-app/) for Android UI/UX which calls `android-rust`


## Building A Release

Detailed instructions:

- [Rust](android-rust/README.md)
- [Android](android-app/README.md)

Due to accepting license agreements, some items to be downloaded from Google
are unable to be fully automated from [this](./), the top-level package.

However, once all dependencies have been met, build a new release of the
actual mobile app:

**1.** Edit [android-app/app/build.gradle](android-app/app/build.gradle)
to reflect the latest Android API level, such as 30.

**2.** Update Rust and Android-Studio packages to latest versions:

    make update

**3.** Build the actual release:

    make release

Along with the resulting .apk file, there will be a "lint" report generated
as an HTML file.  Be sure to follow its recommendations for updating library
versions, etc.

See each subproject's instructions for details.


## Software Architecture

As of May 2019,
[Google recommends](https://android-developers.googleblog.com/2019/05/google-io-2019-empowering-developers-to-build-experiences-on-Android-Play.html)
writing new Android apps using the Kotlin programming language and using
`androidx` frameworks, which has been done here.

There are multiple code repositories, each building upon the next:

1. [Android app in Kotlin](android-app/README.md)
2. [Android NDK layer in Rust](android-rust/) which calls into JNI
3. [Primary "business logic" in Rust](https://github.com/dpezely/anagram-phrases),
   shared with a web service and desktop app elsewhere

By separating concerns this way, each code base may be sufficiently
specialized, and code repositories may then track independent histories that
remain internally consistent.  (Whether to use one giant repo versus several
small ones may be debated elsewhere.)

### Android-specific

Two screens (each an `Activity`) are defined within
[AndroidManifest.xml](android-app/app/src/main/AndroidManifest.xml), which specifies the
parent relationship and facilitates the familiar "back" button (`Up`) to
work via the Android UI.

This app launches by instantiating our
[MainActivity](android-app/app/src/main/java/com/example/anagrams/MainActivity.kt).

**MainActivity** configures the primary input `EditText` field and its
associated palette of `Button`s, all of which are visible on the initial
screen.

Upon tapping buttons from that initial screen and submitting the query, our
app's **MainActivity** instantiates `Intent` and supplies it with our
`query` String via `putExtra()` with key of `"android.intent.action.SEARCH"`
(via our `SEARCH_INTENT` constant).

Instantiating the `Intent` specifies our
[ResultsActivity](android-app/app/src/main/java/com/example/anagrams/ResultsActivity.kt)
which inherits from `SingleFragmentActivity`.  Upon Android invoking our
`override` member function `createFragment()`, it instantiates our
[MainFragment](android-app/app/src/main/java/com/example/anagrams/MainFragment.kt).

Our **MainFragment** inherits from `Fragment` and represents the second
screen of the app: search results page.

Associated with **MainFragment**, a companion object in the same file offers
an alternate constructor, `newInstance(query)`.  It provides the means to
supply a `query` String.

Although **MainFragment** holds a copy of the query, it along with the
actual search results must reside elsewhere, because the Android OS can
destroy and re-create any UI element at any time.  This commonly happens
when a particular `Activity` (or "screen" in the app) goes out of view.

The proper location to store actual data is a `ViewModel`, according to
Android app architecture documentation as of early 2020.

[ResultsViewModel](android-app/app/src/main/java/com/example/anagrams/ResultsViewModel.kt)
inherits from `ViewModel` and becomes our sole repository for actual data.

*This class is the single entry point into our native Rust-based library.*

**ResultsViewModel** calls `System.loadLibrary("rust")` for invoking our
Native code written in the Rust programming language.  That system call gets
performed within a Kotlin companion object's static `init` block associated
with the class, so a future Android `Service` may invoke it independently of
an Activity such as by binding a job.

> As an aside:
>
> Mechanisms such as `MutableLiveData` were deemed unnecessary, as the
> `Observer` model adds too much complexity for a relatively fast operation
> of populating a 2-tier set of nested hash tables, as performed by our Rust
> code.

Also within our **ResultsViewModel** class: 

    external fun query(q: String): Resultset

The external function returns results nested `Lists`.  Complexity of
composing this hierarchy gets resolved entirely from the Native library in
Rust (see [below](#native-library-in-rust)), which invokes JNI calls, making
it completely encapsulated away from Android UI/UX logic.

Kotlin code may simply use the `Resultset` instance as any other JVM object
because it is a JVM object.

Once our **MainFragment** has "inflated" the `RecyclerView`, our class gets
a member field that reflects an identifier within our
[fragment_main.xml](android-app/app/src/main/res/layout/fragment_main.xml) file,
`list_recycler_view`.

That field represents a structure of its own which gets assigned inside
`override fun onViewCreated()` in our code.

Immediately after the Native call into Rust, our `list_recycler_view` gets
populated.  There, a `LinearLayoutManager` and `RecyclerView.Adapter` get
instantiated.  Those two steps create the `RecyclerView` and feed it with
the data returned from our Rust call, respectively.

Since results at this stage must be directly used by the Android Runtime via
[ListAdapter](android-app/app/src/main/java/com/example/anagrams/ListAdapter.kt)'s
`onBindViewHolder()`, this justifies adding complexity to our Rust
invocation of JNI, detailed below.

There are additional bits of Kotlin code best described as "boilerplate"
with respect to the `RecyclerView` such as
[SingleFragmentActivity](android-app/app/src/main/java/com/example/anagrams/SingleFragmentActivity.kt)
for the results screen/activity.

### Native library in Rust

The [corresponding Rust code](android-rust/src/lib.rs) contains a
function prefixed with `pub extern "system"` and
[JNI-specific name
mangling](https://docs.oracle.com/javase/6/docs/technotes/guides/jni/spec/design.html#wp615)
of:

    `Java_` + <this package name> + `_` + <this class name> + `_query(...)`

Some decorators precede that statement, and additional parameters must be
specified:

    #[no_mangle]
    #[allow(non_snake_case)]
    pub extern "system" 
    fn Java_com_example_anagrams_ResultsViewModel_query(
        env: JNIEnv,
        _class: JClass, 
        j_input: JString) -> jobject { ... }

If the calling class from the Kotlin/Java side changes, the above function
name must correspond to the new class name.

> Alternatively, implement
[JNI_OnLoad()](https://developer.android.com/training/articles/perf-jni#native-libraries)
> for populating a list of function names at *run-time* via JNI's
> `register_native_methods()`.
> See [onload.rs](android-rust/src/onload.rs).

> Also, for accommodating anything *older* than Android 6.0 (API level 22 and
> earlier), linker options must change because `--hash-style=gnu` isn't
> supported on those older versions.
> See [android-rust Makefile](android-rust/Makefile).

See crates.io and docs.rs for [JNI](https://crates.io/crates/jni), as other
logistics are required such as [compiling Rust code](android-rust/Makefile)
to several different machine types, copying the shared object (`.so`) files
somewhere that `gradlew` expects them, etc.

*Kotlin may then call the Rust library as any other Kotlin-based function.*

For simplifying the Kotlin code, complexity of populating Kotlin/Java data
structures gets handled by the Rust library.

Conforming to well-defined Java Interfaces (similar enough to Rust's Traits
for purposes here), our Rust code calls `JList::from_env(...)` to
instantiate based upon an earlier Reflection-based look-up for a specific
variations of `ArrayList` constructors.  These are expensive calls due to a
runtime look-up yet without impacting User Experience (UX) on an older
Android 4.2.2 device with modest hardware specs by early 2013 standards.

*Custom `class` or `struct` are avoided when crossing the JNI boundary.*

Instead, nested `ArrayList` each representing either a sequence or tuple are
straight-forward use from each side of the JNI boundary.  (Nested lists
should seem familiar for those acquainted with various dialects of Lisp.)

A Kotlin `typealias` describes each layer of this hierarchy in
[Resultset](android-app/app/src/main/java/com/example/anagrams/Resultset.kt).

Due to Rust code performing JVM actions like Reflection, this Rust library
is just a wrapper that interfaces our *actual* core library,
[lib.rs](https://github.com/dpezely/anagram-phrases/src/lib.rs).

## Contributing

This code is *unlikely* to be maintained by its
[original](https://gitLab.com/dpezely) [author](https://gitHub.com/dpezely)
due to time constraints and a newfound desire for distance from anything
related to Android app development beyond (maybe) integration of a library
written in Rust.

If you fork this code and develop it further, please let me know.  I'll put
a link to your repo at top of this README.  This applies to using a
different Rust library than the one presently used.

## License

MIT License

Essentially, do as you wish and without warranty from authors or maintainers.
