package com.example.anagrams

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

// Create a custom Adapter that will bind our data to
// populated AnagramsViewHolders in the RecyclerView
class ListAdapter(private val results: Resultset):
    RecyclerView.Adapter<AnagramsViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):
            AnagramsViewHolder {
                val inflater = LayoutInflater.from(parent.context)
                return AnagramsViewHolder(inflater, parent)
            }

        override fun onBindViewHolder(holder: AnagramsViewHolder, position: Int) =
            holder.bind(results[position])

        override fun getItemCount(): Int = results.size
}
