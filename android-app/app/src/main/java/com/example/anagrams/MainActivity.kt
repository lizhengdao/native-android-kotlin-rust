package com.example.anagrams

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.text.Typography.nbsp

// https://developer.android.com/reference/kotlin/android/content/Intent#action_search
const val SEARCH_INTENT = "android.intent.action.SEARCH"

class MainActivity: AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        configureEditText()
        configureSubmitButton()
        configureClearButton()
    }

    override fun onStart() {
        super.onStart()
        configureEditText()
    }

    // Initiate a search for `query` within Anagrams.
    // https://developer.android.com/training/basics/firstapp/starting-activity
    // @param query Keywords of search term separated by whitespace.
    private fun searchAnagrams(query: String) {
        val intent = Intent(this, ResultsActivity::class.java).apply {
            if (BuildConfig.DEBUG) {
                Log.d("anagrams", "MainActivity set ${SEARCH_INTENT}=$query")
            }
            putExtra(SEARCH_INTENT, query)
        }
        startActivity(intent)
    }

    // Set focus to EditText input box, so soft keyboard may appear.
    private fun configureEditText() {
        val queryText = findViewById<EditText>(R.id.query_input)
        queryText?.requestFocus()
    }

    private fun configureSubmitButton() {
        val btnSubmit = findViewById<Button>(R.id.submit_button)
        btnSubmit?.setOnClickListener {
            val queryText = findViewById<EditText>(R.id.query_input)
            queryText?.getText().toString().let {
                if (it.length > 0) {
                    if (BuildConfig.DEBUG) {
                        Log.d("anagrams", "MainActivity button=submit query=$it")
                    }
                    Toast.makeText(this@MainActivity,
                                   getString(R.string.processing),
                                   Toast.LENGTH_LONG).show()
                    searchAnagrams(it)
                } else {
                    if (BuildConfig.DEBUG) {
                        Log.d("anagrams", "MainActivity ignoring empty query")
                    }
                }
            }
        }
    }

    private fun configureClearButton() {
        val btnClear = findViewById<Button>(R.id.clear_button)
        btnClear?.setOnClickListener {
            val queryText = findViewById<EditText>(R.id.query_input)
            queryText?.setText("")
            if (BuildConfig.DEBUG) {
                Log.d("anagrams", "Query cleared")
            }
        }
    }
}
