package com.example.anagrams

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

// Create a custom ViewHolder
class AnagramsViewHolder(inflater: LayoutInflater, parent: ViewGroup):
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item, parent, false)) {
        private var mPhraseView: TextView? = null

        init {
            mPhraseView = itemView.findViewById(R.id.list_phrase)
        }

        fun bind(phrase: String) {
            if (BuildConfig.DEBUG) {
                Log.d("anagrams", "AnagramsViewHolder bind: $phrase")
            }
            mPhraseView?.text = phrase
        }
    }
